import subprocess

def run_test(test_input):
    process = subprocess.Popen(["./result"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=test_input.encode())

    return stdout.decode().strip(), stderr.decode().strip()

def main():
    test_cases = [
        ("fir", "value1", ""),
        ("sec", "value2", ""),
        ("thi", "value3", ""),
        ("for", "value4", ""),
        ("fif", "value5", ""),
        ("", "", "found nothing"),
        ("Hello, there", "", "found nothing"),
        ("General Kenobi", "", "found nothing")
    ]

    num_failures = 0
    print("Starting tests")
    print("-------------------")

    for i, (test_input, expected_output, expected_error) in enumerate(test_cases):
        actual_output, actual_error = run_test(test_input)

        if actual_output == expected_output and actual_error == expected_error:
            print(f"Test {i} passed")
        else:
            num_failures += 1
            print(f"Test {i} failed")
            if actual_output != expected_output:
                print(f"Wrong stdout: '{actual_output}', expected: '{expected_output}'")
            if actual_error != expected_error:
                print(f"Wrong stderr: '{actual_error}', expected: '{expected_error}'")
        print("-------------------")

    if num_failures == 0:
        print("All tests passed")
    else:
        print(f"{num_failures} tests failed")

if __name__ == "__main__":
    main()
