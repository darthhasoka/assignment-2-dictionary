%include "colon.inc"
%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define DATA_OFFSET 8 ; Смещение данных в узле

global _start

section .bss
buffer: resb BUFFER_SIZE

section .rodata
msg_input_error: db "key must be less than 255 characters!", 0
msg_finding_error: db "found nothing", 0

section .text
_start:
    ; Чтение слова из stdin в buffer
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    jz .handle_input_error

    ; Поиск слова в словаре
    mov rdi, buffer
    mov rsi, dict
    call find_word
    test rax, rax
    jz .handle_finding_error

    ; Вывод найденного слова
    mov rdi, rax
    add rdi, DATA_OFFSET
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    jmp .exit

.handle_input_error:
    mov rdi, msg_input_error
    jmp .display_error

.handle_finding_error:
    mov rdi, msg_finding_error
    jmp .display_error
.display_error:
    call print_error
.exit:
    call exit
