ASM=nasm
ASMFLAGS=-f elf64
LINKER=ld
LFLAGS=

.PHONY: tests clean

result: main.o lib.o dict.o
	$(LINKER) $(LFLAGS) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm dict.inc colon.inc lib.inc words.inc
dict.o: dict.asm lib.inc

tests: result
	python3 test.py
	rm -f result

clean:
	rm -rf *.o
